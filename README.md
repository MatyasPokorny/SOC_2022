# Probe measurements of edge plasma properties at the GOLEM tokamak using a motorised manipulator


This repository contains my SOČ (*[Středoškolská Odborná Činnost](https://www.soc.cz/)*) work conducted at the [GOLEM tokamak](http://golem.fjfi.cvut.cz/wiki/) during the year 2021/22. Me and Ing. Petr Mácha, my consultant, put into operation a new motorised electrical probe manipulator, which we then used to conduct a measurement of both the perpendicular and parallel components of the Mach number desribing plasma velocity (i.e., how fast the plasma moves in the toroidal direction and the radial direction).

The repository contains the following files:

- `SOC2022_MatyasPokorny.pdf` containing the SOČ work itself
- `SOC2022_priloha.py` containing a selection of important Python scipts written while conducting the work

Based on the work, an [article](https://rozhledy.jcmf.cz/wp-content/uploads/RMF-97-3.pdf), published in the journal *ROZHLEDY matematicko-fyzikální*, was written.

---

### Current state of the work

The motorised manipulator is fully functional. The user operates it remotely, via a web application. So far, he has to read both the radial and axial coordinates of his probe from blurry camera images, which isn't very practical. At the moment, we are building a system, based on machine learning, which will read the coordinates by itself (the project doesn't have a high priority, so it might take a while).

No more measurements of the Mach number were conducted at the GOLEM tokamak after this work.
