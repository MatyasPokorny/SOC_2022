# Řádek #XXXXX... znázorňuje příkaz pro vytvoření grafu


###############################################################################
# IMPORT VYUŽITÝCH KNIHOVEN: --------------------------------------------------


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import os 


###############################################################################
# DEFINICE FUNKCÍ: ------------------------------------------------------------

def get_signal(shot_num, channel):
    """
    Function "get_signal" gives out mesured signal indexed by time for a given shot at a given channel
    
    Output type = pd.Series
    """
    data_name = f"04_01_22_loaded_shots_{shot_num}_{channel}.csv"
    data_path = f"/home/maty/Plocha/TokGol_2021_22/python/golem_sessions/DATA/13_02_22/SIGNAL/{data_name}"
    
    if os.path.exists(data_path) == False:
    
        try:
            
            data = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shot_num}/Diagnostics/TunnelProbe/DAS_raw_data_dir/TektrMSO64_ALL.csv", skiprows=(10))
            df = pd.DataFrame(data)
            desired_data = pd.Series(df[str(channel)])
            # print(f"Data downloaded; shot_num = {shot_num}; channel = {channel}")
            
            if str(channel) == "CH1":
                desired_data = desired_data/100    
            elif str(channel) == "CH2":
                desired_data = desired_data/100
            elif str(channel) == "CH3":
                desired_data = desired_data/100
            elif str(channel) == "CH4":
                desired_data = desired_data/100
            # print("Data shows I_probe")
            
            TIME = pd.Series(df["TIME"])*1000
            desired_data.index = TIME
            # print("Data indexed by time")
            
            # url_min = f"http://golem.fjfi.cvut.cz/shots/{shot_num}/Diagnostics/BasicDiagnostics/Results/t_plasma_start"
            # url_max = f"http://golem.fjfi.cvut.cz/shots/{shot_num}/Diagnostics/BasicDiagnostics/Results/t_plasma_end"
            # t_min = np.genfromtxt(url_min)
            # t_max = np.genfromtxt(url_max)
            # print("Plasma duration downloaded")
        
            # while desired_data.index[0] < t_min:
            #     desired_data = desired_data.drop(desired_data.index[0])
            # print("t_min boundry set")
            # while desired_data.index[-1] > t_max:
            #     desired_data = desired_data.drop(desired_data.index[-1])
            # print("t_max boundry set")
        
            # print(f"signal {shot_num} {channel} loaded")
            
            desired_data.to_csv(data_path)
           
            return desired_data
    
        except:
            
            print(f"signal of shot num {shot_num} and channel {channel} not found")
            return []
        
    else:
        
        data = pd.read_csv(data_path)
        df = pd.DataFrame(data)
        
        desired_data = pd.Series(df[data.columns[1]])
        desired_data.index = df["TIME"]
        
        
        # print(f"signal {shot_num} {channel} loaded")    
        
        return desired_data

def get_basic_diag(shot_num, diag):
    """
    Function "get_basic_diag" returns the requested basic diagnostic indexed by time for a given shot
    
    Input:
        U_loop
        Bt
        Ip
    
    Output type = pd.Series
    """
    data_name = f"04_01_22_loaded_basic_diag_{shot_num}_{diag}.csv"
    data_path = f"/home/maty/Plocha/TokGol_2021_22/python/golem_sessions/DATA/13_02_22/BASIC_DIAG/{data_name}"

    if os.path.exists(data_path) == False:
        
        try:
            
            url = f"http://golem.fjfi.cvut.cz/shots/{shot_num}/Diagnostics/BasicDiagnostics/Results/{diag}.csv"
            data = pd.read_csv(url)
            requested_diag = data[str(data.columns[1])]
            requested_diag.index = data[str(data.columns[0])]
        
            requested_diag.to_csv(data_path)
        
            return requested_diag
        
        except:
        
            print(f"data of shot num {shot_num} and basic diag {diag} not found")
            return []
        
    else:
        
        data = pd.read_csv(data_path)

        requested_diag = data[str(data.columns[1])]
        requested_diag.index = data[str(data.columns[0])]

        return requested_diag    



global t_values_arr

def calculate_I_sat(t_min, t_max, t_step):
    """
    Input:
    -------   
        t_min and t_max are boudaries set to total time interval on which we calculate I_sat
        t_step is the increment of time and sets number of intervals in total time interval
        everything in [ms]
    
    Returns:
    -------
    I_sat_avg_values_arr : array (of arrays)
        
        The returned array contains an array of I_sat values for each chosen time
        
        len(I_sat_avg_values_arr) = number of chosen times
        len(I_sat_avg_values_arr[x]) = number of angles resp. shots in series

    """
    I_sat_arr = []
    for i in range(len(shot_nums)):
        current_I_sat = get_signal(shot_nums[i], "CH2") + get_signal(shot_nums[i], "CH3")
        I_sat_arr.append(current_I_sat)

    for i in range(len(shot_nums)):
        current_I_sat = get_signal(shot_nums[i], "CH4") + get_signal(shot_nums[i], "CH1")
        I_sat_arr.append(current_I_sat)
    
    
    #Zde je sestrojen array časů, pro které chceme I_sat vypočítat
    global t_values_arr
    t_values_arr = []
    x = t_min
    while x <= t_max:
        t_values_arr.append(x)
        x = x + t_step
    
    #Zde je sestrojen finální I_sat_avg_values_arr
    I_sat_avg_values_arr = []
    for j in range(len(t_values_arr)):
        current_t = t_values_arr[j]
        current_t_values = []
        for i in range(len(I_sat_arr)):
            current_I_sat = I_sat_arr[i]
            current_value = current_I_sat[current_t:current_t + t_step].mean() * 1000
            current_t_values.append(current_value)
        I_sat_avg_values_arr.append(current_t_values)
    
    return I_sat_avg_values_arr


def M_par(x_1):
    """
    ------------------
    Input:
        x_1 is a position of orientaton alpha_p = 90° in I_sat_arr
    
    ------------------
    Returns:
        Array of M_par values at chosen time intervals in I_sat_arr
    
    """
    M_par_arr = []
    
    for i in range(len(I_sat_arr)):
        x_2 = x_1 + 22
        if x_2 > 44:
            x_2 = x_2 - 44
        R = I_sat_arr[i][x_2] / I_sat_arr[i][x_1]
        M_par = K * np.log(R)
        M_par_arr.append(M_par)
        
    return M_par_arr


def M_perp(M_par):
    """
    ------------------
    Input:
        M_par is the value of the parallel component of the Mach number
    
    ------------------
    Returns:
        Array of M_perp values at each angle of the axial profile
    
    """
    M_perp_arr = []
    
    for i in range(len(angle_arr_rad)):
                
            i_2 = i + 22
            if i_2 >= 44:
                i_2 = i_2 - 44
            
            R = I_sat_arr[0][i_2] / I_sat_arr[0][i]

            M_perp =  (np.tan(angle_arr_rad[i])) * (M_par - K * np.log(R))
            M_perp_arr.append(M_perp)
            
    return M_perp_arr


def calculate_R(I_sat_arr, angle):
    """
    ------------------
    Input:
        I_sat_arr contains axial profile of ion saturated current
        angle is the position of I_sat of R at desired angle in I_sat_arr
    ------------------
    Returns:
        R ( = I_sat1/I_sat_2) at a given angle (resp. position in I_sat_arr)
    
    """
    pos_1 = angle_arr.index(angle)
    pos_2 = pos_1 + 22
    
    if pos_2 > 44:
        pos_2 = pos_2 - 44
    
    R = I_sat_arr[pos_1] / I_sat_arr[pos_2]
    return R

###############################################################################
# DEFINICE VYUŽITÝCH PROMĚNNÝCH: ----------------------------------------------

# -----
# 1. primární výbojová série:
# -----

angle_array = [0, 10, 20, 30, 40, 50, 60, 80, 90, 130, 150, 170, 180, 190, 200, 210, 220, 230, 240, 260, 270, 310, 330, 350] #[°]

shot_array = [37956, 37957, 37958, 37959, 37960, 37961, 37962, 37963, 37964, 37965, 37966, 37967]



# -----
# 2. primární výbojová série:
# -----


angle_arr = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 145, 150, 155, 160, 165, 170, 180, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 325, 330, 335, 340, 345, 350, 360]

shot_nums = [38061, 38062, 38063, 38064, 38065, 38066, 38067, 38068, 38069, 38070, 38071, 38072, 38073, 38074, 38075, 38082, 38076, 38081, 38077, 38080, 38078, 38079]


###############################################################################
# PŘÍKLAD VIZUALIZACE DAT: ----------------------------------------------------

# Základní diagnostiky první výbojové série:--------------------------------

plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "sans-serif"
plt.rcParams['font.sans-serif'] = "Helvetica"

#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
fig, axs = plt.subplots(3, 1, figsize = (15,10))

for i in range(len(shot_array)):
    axs[0].plot(get_basic_diag(shot_array[i], "U_loop"), linewidth=3)

axs[0].set_ylabel("$U_{loop} \:\:\: \mathrm{[V]}$", fontsize=45, labelpad=40)
axs[0].set_title(r"{\#37956 - \#37967}", fontsize=45)
axs[0].set_xticks([0, 5, 10, 15, 20, 25])
axs[0].set_xticklabels([])
axs[0].set_yticks([0, 5, 10, 15, 20])
axs[0].tick_params(axis="y", labelsize=35)
axs[0].grid(True, alpha = 0.5)
axs[0].axvspan(8, 10, color="blue", alpha=0.1)



for i in range(len(shot_array)):
    axs[1].plot(get_basic_diag(shot_array[i], "Ip"), linewidth=3)
    
axs[1].set_ylabel("$I_{p} \:\:\: \mathrm{[kA]}$", fontsize=45, labelpad=51)
axs[1].set_xticks([0, 5, 10, 15, 20, 25])
axs[1].set_xticklabels([])
axs[1].set_yticks([0.0, 1.0, 2.0, 3.0])
axs[1].tick_params(axis="y", labelsize=35)
axs[1].grid(True, alpha = 0.5)
axs[1].axvspan(8, 10, color="blue", alpha=0.1)



for i in range(len(shot_array)):
    axs[2].plot(get_basic_diag(shot_array[i], "Bt"), linewidth=3)

axs[2].set_xlabel("$t \; \mathrm{[ms]}$", fontsize=47)
axs[2].tick_params(axis="x", labelsize=35) 
axs[2].set_ylabel("$B_{t} \; \mathrm{[T]}$", fontsize=45, labelpad=30)
axs[2].set_yticks([0.0, 0.2, 0.4, 0.6])
axs[2].set_xticks([0, 5, 10, 15, 20, 25])
axs[2].tick_params(axis="y", labelsize=35)
axs[2].grid(True, alpha = 0.5)
axs[2].axvspan(8, 10, color="blue", alpha=0.1)



fig.tight_layout()
plt.show()
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX



# Polární graf I_sat druhé výbojové série:----------------------------------


I_sat_arr = []
for i in range(len(shot_nums)):
    current_I_sat = get_signal(shot_nums[i], "CH2") + get_signal(shot_nums[i], "CH3")
    I_sat_arr.append(current_I_sat)

for i in range(len(shot_nums)):
    current_I_sat = get_signal(shot_nums[i], "CH4") + get_signal(shot_nums[i], "CH1")
    I_sat_arr.append(current_I_sat)

t_step = 0.5
t_min = 8.5
t_max = 10

I_sat_arr = calculate_I_sat(t_min, t_max, t_step)

angle_arr_rad = np.radians(angle_arr)


#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
fig = plt.figure(figsize = (20,10))

plt.axes(projection = "polar")

plt.title(r"\#38061 - \#38079", fontweight = "bold", fontsize = 40)

for i in range(len(I_sat_arr)):
    plt.scatter(angle_arr_rad, I_sat_arr[i], s=150, marker="x")
    plt.plot(angle_arr_rad, I_sat_arr[i], linewidth = 3, label=f"$t = {round(t_values_arr[i], 3)} \; \mathrm{{ms}}$")

plt.tick_params(axis = "y", labelsize=36)
plt.tick_params(axis = "x", labelsize=40)

plt.yticks([25, 50, 100, 125])

plt.legend(fontsize = 25, loc=(-0.2,0.165))
plt.grid(alpha = 0.5)
plt.show()
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


###############################################################################
# MACHOVO ČÍSLO: --------------------------------------------------------------


K = 0.43



# Časová závislost M_par pro orientaci sondy paralelní s magnetickými siločárami:

t_min = 5
t_max = 14
t_step = 0.5

I_sat_arr = calculate_I_sat(t_min, t_max, t_step)
M_par_arr = M_par(31)
t_values_arr_1 = t_values_arr

t_min = 5
t_max = 14
t_step = 0.01

I_sat_arr = calculate_I_sat(t_min, t_max, t_step)
M_par_arr_2 = M_par(31)
t_values_arr_2 = t_values_arr




#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
fig = plt.figure(figsize = (20,10))

plt.errorbar(t_values_arr_1, M_par_arr, yerr = np.std(M_par_arr), color="blue", linewidth = 4, alpha = 0.5, elinewidth=1.5, label=r"Časový interval $\Delta t = 0.5 \; \mathrm{ms}$")
plt.scatter(t_values_arr_1, M_par_arr, s=400, color = "red", marker="x")

plt.plot(t_values_arr_2, M_par_arr_2, linewidth = 2.5, color="orange", alpha=0.29, label=r"Časový interval $\Delta t = 0.01 \; \mathrm{ms}$")

plt.title(r"$\alpha_p = 90,140^\circ$", fontsize = 40)
plt.xlabel(r"$t \; \mathrm{[ms]}$", fontsize = 40, labelpad = 30)
plt.ylabel(r"$M_\parallel \; [-]$", fontsize = 40)

plt.tick_params(axis="both", labelsize=34)

plt.legend(fontsize=30)
plt.grid(alpha = 0.5)
plt.show()
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


# Výpočet Machova čísla podle první metody: -----------------------------------

t_min = 8
t_max = 9
t_step = 0.5                      # Časový interval maxima M_par při kvazistacionární fázi

I_sat_arr = calculate_I_sat(t_min, t_max, t_step)

M_par1 = M_par(31)
M_par_t1 = 0.17923161983420047    # Získaná hodnota z předešlé řádky
M_par_t1_std = np.std(M_par1)     # Standardní odchylka

M_par_t1_arr = []                 # Aby jej bylo možné vizualizovat v grafu
for i in range(len(angle_arr)):
    M_par_t1_arr.append(M_par_t1)





M_perp_t1 = M_perp(M_par_t1)

for i in range(len(M_perp_t1)):   # Aby bylo možné vypočítat odchylku
    if abs(M_perp_t1[i]) < 0.000001 or abs(M_perp_t1[i]) > 10000:
        M_perp_t1[i] = 0

M_perp_t1_std = np.std(M_perp_t1)

M_perp_t1[9] = None               # Odstranění divergence
M_perp_t1[31] = None





#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
fig = plt.figure(figsize = (20, 13))

plt.errorbar(angle_arr, M_par_t1_arr, label = r"$M_\parallel$", linewidth=3)
plt.plot(angle_arr, M_perp_t1, label = r"$M_\perp$", linewidth=3)
plt.scatter(angle_arr, M_perp_t1, s=100, color="red", marker="x")

plt.errorbar(angle_arr[0:8], M_perp_t1[0:8], yerr = M_perp_t1_std, color = "orange", elinewidth=1.5)
plt.errorbar(angle_arr[10:30], M_perp_t1[10:30], yerr = M_perp_t1_std, color = "orange", elinewidth=1.5)
plt.errorbar(angle_arr[32:44], M_perp_t1[32:44], yerr = M_perp_t1_std, color = "orange", elinewidth=1.5)

plt.title(r"\#38061 - \#38079; $t_1 = [8; \; 9] \; \mathrm{ms}$", fontsize = 40)
plt.xlabel(r"$\alpha_p \;\; \mathrm{[ \; {}^\circ \;]}$", fontsize = 40, labelpad = 30)
plt.ylabel(r"$M \;\; [-]$", fontsize = 40)

plt.xticks(np.arange(0, 361, 45))
plt.tick_params(axis="both", labelsize=34)

plt.axvline(90, color="red", linestyle="dashed", linewidth = 3)
plt.axvline(270, color="black", linestyle="dashed", linewidth = 3)
plt.axvspan(0, 60, color="orange", alpha = 0.14)
plt.axvspan(120, 240, color="orange", alpha = 0.14)
plt.axvspan(300, 360, color="orange", alpha = 0.14)
plt.text(140, 1, f"$M_{{\parallel}} = {round(M_par_t1, 2)}$", fontsize = 50, color = "blue")


plt.legend(fontsize = 35)
plt.grid(alpha = 0.5)
plt.show()
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX









t_min = 10
t_max = 10.5
t_step = 0.5                       # Časový interval minima M_par při kvazistacionární fázi

I_sat_arr = calculate_I_sat(t_min, t_max, t_step)

M_par2 = M_par(31)
M_par_t2 = -0.08214842362503974    # Získaná hodnota z předešlé řádky
M_par_t2_std = np.std(M_par2)      # Standardní odchylka

M_par_t2_arr = []                  # Aby jej bylo možné vizualizovat v grafu
for i in range(len(angle_arr)):
    M_par_t2_arr.append(M_par_t2)





M_perp_t2 = M_perp(M_par_t2)

for i in range(len(M_perp_t2)):    # Aby bylo možné vypočítat odchylku
    if abs(M_perp_t2[i]) < 0.000001 or abs(M_perp_t2[i]) > 10000:
        M_perp_t2[i] = 0

M_perp_t2_std = np.std(M_perp_t2)

M_perp_t2[9] = None                # Odstranění divergence
M_perp_t2[31] = None




#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
fig = plt.figure(figsize = (20, 13))

plt.errorbar(angle_arr, M_par_t2_arr, label = r"$M_\parallel$", linewidth=3)
plt.plot(angle_arr, M_perp_t2, label = r"$M_\perp$", linewidth=3)
plt.scatter(angle_arr, M_perp_t2, s=100, color="red", marker="x")

plt.errorbar(angle_arr[0:8], M_perp_t2[0:8], yerr = M_perp_t2_std, color = "orange", elinewidth=1.5)
plt.errorbar(angle_arr[10:30], M_perp_t2[10:30], yerr = M_perp_t2_std, color = "orange", elinewidth=1.5)
plt.errorbar(angle_arr[32:44], M_perp_t2[32:44], yerr = M_perp_t2_std, color = "orange", elinewidth=1.5)

plt.title(r"\#38061 - \#38079; $t_2 = [10; \; 10,5] \; \mathrm{ms}$", fontsize = 40)
plt.xlabel(r"$\alpha_p \;\; \mathrm{[ \; {}^\circ \;]}$", fontsize = 40, labelpad = 30)
plt.ylabel(r"$M \;\; [-]$", fontsize = 40)

plt.xticks(np.arange(0, 361, 45))
plt.tick_params(axis="both", labelsize=34)

plt.axvline(90, color="red", linestyle="dashed", linewidth = 3)
plt.axvline(270, color="black", linestyle="dashed", linewidth = 3)
plt.axvspan(0, 60, color="orange", alpha = 0.14)
plt.axvspan(120, 240, color="orange", alpha = 0.14)
plt.axvspan(300, 360, color="orange", alpha = 0.14)
plt.text(140, 0.6, f"$M_{{\parallel}} = {round(M_par_t2, 2)}$", fontsize = 50, color = "blue")


plt.legend(fontsize = 35)
plt.grid(alpha = 0.5)
plt.show()
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX



# Výpočet Machova čísla podle druhé metody: -------------------------------


t_min = 8.5
t_max = 8.5
t_step = 2                   # Časový inerval kvazistacionární fáze

I_sat_arr = calculate_I_sat(t_min, t_max, t_step)[0]

ln_R_arr = []
for i in range(22):
    R = calculate_R(I_sat_arr, angle_arr[i])
    ln_R_arr.append(np.log(R))
    
def M_func(alpha_p, M_par, M_perp):
    return (  M_par - (  M_perp * np.cos(alpha_p)/np.sin(alpha_p)  )  ) / 2.3

popt, pcov = curve_fit(M_func, angle_arr_rad[2:19], ln_R_arr[2:19])

angle_arr_rad[21] = None
angle_arr_rad[22] = None





#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
fig = plt.figure(figsize = (18, 9))

plt.scatter(angle_arr[1:21], ln_R_arr[1:21], s = 300, marker = "x", color="red", label = r"$\mathrm{ln}(R_{\alpha_p})$")
plt.plot(angle_arr[1:21], ln_R_arr[1:21], linewidth = 2, linestyle = "dotted", color="red", alpha = 0.7)
plt.plot(angle_arr[2:19], M_func(angle_arr_rad[2:19], popt[0], popt[1]), linewidth = 3, label = r"$ M_\parallel =  0.411 \pm 0.036 \\ M_\perp = -0.141 \pm 0.026 $")

plt.title(r"\#38062 - \#38078; $t = [8,5; \; 10,5] \; \mathrm{ms}$", fontsize = 40)
plt.xlabel(r"$\alpha_p \; [ \; {}^\circ \; ]$", fontsize = 40, labelpad = 10)
plt.ylabel(r"$\mathrm{ln}(R_{\alpha_p}) \; \mathrm{[-]}$", fontsize = 40)

plt.xticks(np.arange(0, 181, 20))
plt.yticks(np.arange(-0.1, 0.6, 0.1))
plt.tick_params(axis = "both", labelsize = 32)

plt.legend(fontsize = 30)
plt.grid(alpha = 0.5)
plt.show
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX













